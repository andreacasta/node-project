DROP SEQUENCE IF EXISTS crud.users;

CREATE TABLE users (
  id INT,
  username VARCHAR(200) UNIQUE NOT NULL,
  password VARCHAR(200) NOT NULL,
  
  PRIMARY KEY (id)
);

