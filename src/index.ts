import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

const express = require('express')
const app = express()
const port = 3000;


AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const user = new User()
    user.id=1
    user.username = "Timber"
    user.password = "admin"
    await AppDataSource.manager.save(user)
    console.log("Saved a new user with id: " + user.id)

    console.log("Loading users from the database...")
    const users = await AppDataSource.manager.find(User)
    console.log("Loaded users: ", users)


}).catch(error => console.log(error))


app.use(express.json())

app.listen(port, (): void => {
    console.log(`Express is listening at http://localhost:${port}`);
});