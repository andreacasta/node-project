import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class User {

    @PrimaryGeneratedColumn({
        type: "int"
    })
    id: number

    @Column({
        unique: true,
        nullable: true,
        type: "varchar",
        length: 200,
    })
    username: string
    

    @Column({
        nullable: true,
        type: "varchar",
        length: 200,
    })
    password: string

   

}
